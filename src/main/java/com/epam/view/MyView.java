package com.epam.view;

import com.epam.model.Human;
import com.epam.model.MyGeneric;
import com.epam.annotation.MyAnno;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

public class MyView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Human obj;
    private static BufferedReader reader =
            new BufferedReader(new InputStreamReader(System.in));

    public MyView() {
        obj = new Human();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Print fields that were annotate by annotation.");
        menu.put("2", "  2 - Invoke methods.");
        menu.put("3", "  3 - Set value into field.");
        menu.put("4", "  4 - Invoke methods with variable number of parameters");
        menu.put("5", "  5 - Test Generic");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::getAnnotatedFields);
        methodsMenu.put("2", this::invokePersonMethods);
        methodsMenu.put("3", this::setValueIntoField);
        methodsMenu.put("4", this::invokeMethodsWithVariableNumberOfParameters);
        methodsMenu.put("5", this::testGenericClass);
    }

    private void printMenu() {
        System.out.println("MENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void start() throws IOException {
        String choice = "";
        while (!choice.equals("Q")) {
            System.out.println("-------------------------------------------");
            printMenu();
            System.out.println("Please, select menu point.");
            System.out.println("-------------------------------------------");
            choice = reader.readLine().toUpperCase();
            Printable printable = methodsMenu.get(choice);
            if (printable != null) {
                printable.print();
            }
        }
    }

    private void getAnnotatedFields() {
        Class clazz = Human.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnno.class)) {
                System.out.println("field: " + field.getName());
                MyAnno annotation = field.getAnnotation(MyAnno.class);
                String annotationSimpleName =
                        annotation.toString()
                                .substring(annotation.toString().lastIndexOf(".") + 1);
                System.out.printf("@%s%n", annotationSimpleName);
            }
        }
    }

    private void invokePersonMethods() {
        Class clazz = Human.class;
        try {
            Method method1 =
                    clazz.getDeclaredMethod(
                            "changePersonData", String.class, String.class, int.class);
            method1.setAccessible(true);
            Human person =
                    (Human) method1.invoke(obj, "Тарас", "Кацьора", 21);
            System.out.println(person);
            System.out.println("----------------------------------------");

            Method method2 =
                    clazz.getDeclaredMethod("spendSomeMoney", int.class);
            method2.setAccessible(true);
            int money = (int) method2.invoke(obj, 50);
            System.out.println("money: " + money);
            System.out.println("----------------------------------------");

            Method method3 =
                    clazz.getDeclaredMethod("printInfo");
            method3.setAccessible(true);
            method3.invoke(obj);
            System.out.println("----------------------------------------");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void setValueIntoField() {
        Class clazz = Human.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.getName().equals("money") && field.getType() == int.class) {
                System.out.println("Set 1000 into money field");
                try {
                    field.setInt(obj, 1000);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("----------------------------------------");
            }
        }
    }

    private void invokeMethodsWithVariableNumberOfParameters() {
        Class clazz = obj.getClass();
        try {
            Method myMethod1 = clazz.getDeclaredMethod(
                    "myMethod", String.class, int[].class);
            myMethod1.setAccessible(true);
            int[] array = {5, 10, 20};
            int sum = (int) myMethod1.invoke(obj, "Hours", array);
            System.out.println(sum);

            Method myMethod2 = clazz.getDeclaredMethod(
                    "myMethod", String[].class);
            myMethod2.setAccessible(true);
            String[] strArray = {"Hello1", "Hello2", "Hello3"};
            myMethod2.invoke(obj, (Object) strArray);
        } catch (InvocationTargetException | IllegalAccessException
                | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    private void testGenericClass() {
        new MyGeneric<>(String.class);
    }
}
