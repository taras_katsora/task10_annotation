package com.epam.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MyGeneric<T> {

    private T obj;
    private final Class<T> clazz;

    public MyGeneric(Class<T> clazz) {
        this.clazz = clazz;
        try {
            obj = clazz.getConstructor().newInstance();
            System.out.println("Name: \n" + clazz.getName());
            System.out.println("------------------------------------");
            System.out.println("Simple name: \n" + clazz.getSimpleName());
            System.out.println("------------------------------------");

            System.out.println("Declared constructors:");
            Constructor<?>[] constructors = clazz.getDeclaredConstructors();
            for (Constructor c : constructors) {
                System.out.printf(" name: %s%n parameter count: %d",
                        c.getName(), c.getParameterCount());
                System.out.print(" parameter types:");
                for (Class type : c.getParameterTypes()) {
                    System.out.print(type.getSimpleName() + " ");
                }
                System.out.println();
                System.out.println("------------------");
            }
            System.out.println("------------------------------------");
            System.out.println("Declared methods:");
            Method[] methods = clazz.getDeclaredMethods();
            for (Method m : methods) {
                System.out.println(m.getName());
            }
            System.out.println("------------------------------------");

            System.out.println("Declared fields:");
            Field[] fields = clazz.getDeclaredFields();
            for (Field f : fields) {
                System.out.println(f.getName());
            }
        } catch (InstantiationException | IllegalAccessException
                | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
}
