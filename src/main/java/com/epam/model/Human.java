package com.epam.model;

import com.epam.annotation.MyAnno;
import java.util.Arrays;

public class Human {

    @MyAnno("Taras")
    private String firstName;
    @MyAnno
    private String surname;
    private int age;
    private int money;

    public Human() {
        this.firstName = "Taras";
        this.surname = "Katsora";
        this.age = 21;
        this.money = 0;
    }

    private Human(String firstName, String surname, int age) {
        this.firstName = firstName;
        this.surname = surname;
        this.age = age;
        this.money = 100;
    }

    private Human changePersonData(String firstName, String surname, int age) {
        System.out.println("Change Person data....");
        this.firstName = firstName;
        this.surname = surname;
        this.age = age;
        return this;
    }

    private int spendSomeMoney(int value) {
        if (money >= value) {
            System.out.println("Spend some money: -" + value);
            money = money - value;
            return money;
        }
        System.out.println("Not enough money");
        return money;
    }

    private void printInfo() {
        System.out.printf("first name: %s%n", firstName);
        System.out.printf("surname: %s%n", surname);
        System.out.printf("age: %d%n", age);
        System.out.printf("money: %d%n", money);
    }

    private int myMethod(String a, int... args) {
        System.out.println("Calculation the amount of " + a);
        return Arrays.stream(args).sum();
    }

    private void myMethod(String... args) {
        System.out.println("Make a speech:");
        for (String str : args) {
            System.out.println("\t" + str);
        }
    }

    @Override
    public String toString() {
        return "Human{" +
                "firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", money=" + money +
                '}';
    }
}

